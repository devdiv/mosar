FROM openjdk:11.0.11-jdk-slim as runtime
WORKDIR /app
COPY target/ ./target/
RUN mv ./target/gameservice*SNAPSHOT.jar ./target/game.jar
ENV SPRING_PROFILES_ACTIVE=aws
ENV SPRING_DATASOURCE_USERNAME=bla
ENV SPRING_DATASOURCE_PASSWORD=bla
CMD ["sh","-c","java -jar ./target/game.jar"]



