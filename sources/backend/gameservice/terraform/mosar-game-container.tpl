[
  {
    "name": "game",
    "image": "${GAME_IMAGE}:${GAME_IMAGE_TAG}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "mosar-game-${ENVIRONMENT}-service",
        "awslogs-group": "${MOSAR_LOG_GROUP_NAME}"
      }
    },
    "portMappings": [
      {
        "containerPort": 8080,
        "hostPort": 8080,
        "protocol": "tcp"
      }
    ],
    "cpu": 1,
    "environment": [
      {
        "name": "SPRING_PROFILES_ACTIVE",
        "value": "${SPRING_PROFILES_ACTIVE}"
      }
    ],
    "secrets": [
    ],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 1024,
    "volumesFrom": []
  }
]
