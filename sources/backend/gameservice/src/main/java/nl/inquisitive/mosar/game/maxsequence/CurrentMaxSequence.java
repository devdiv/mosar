package nl.inquisitive.mosar.game.maxsequence;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Collection;

public class CurrentMaxSequence {

    //Tread safe double locked singleton
    private static CurrentMaxSequence uniqueCurrentMaxSequence;

    @Getter
    @Setter
    private Integer currentFlashcardMaxSequence;


    Collection<Object[]> allMaxValuesOfAllSets = new ArrayList<>();


    public Collection<Object[]> getAllMaxValuesOfAllSets(){
        return allMaxValuesOfAllSets;
    }

    public void setAllMaxValuesOfAllSets(Collection<Object[]> input){
      this.allMaxValuesOfAllSets = input;
    }

    public static CurrentMaxSequence getInstance() {
        if (uniqueCurrentMaxSequence == null) {
            synchronized (CurrentMaxSequence.class) {
                if (uniqueCurrentMaxSequence == null) {
                    uniqueCurrentMaxSequence = new CurrentMaxSequence();
                }
            }
        }
        return uniqueCurrentMaxSequence;
    }
}
