package nl.inquisitive.mosar.game.maxsequence;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;

@Slf4j
@Component
public class SetupAndUpdateMaxSequence {

  private static final HttpClient HTTP_CLIENT = HttpClient.newBuilder()
      .version(HttpClient.Version.HTTP_1_1)
      .connectTimeout(Duration.ofSeconds(10))
      .build();

  @Getter
  @Setter
  public int maxValueOfASet;

  @Getter
  @Setter
  public int flashcardReturnStatusCode;

  @Getter
  @Setter
  public boolean requestFromMemory = true;

  private String URL_FLASHCARD;

  @Autowired
  public SetupAndUpdateMaxSequence(@Value("${url.flashcard}") String urlFlashcard) {
    this.URL_FLASHCARD = urlFlashcard;
  }

  // Object that fills the singleton
  public void singletonAllMaxValuesOfAllSetsHandler(String setId) {
    // If currentmax is empty, just run the request
    if (CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets().isEmpty()) {
      requestAndParseWrapper(setId);
    } else {
      Object[] arrayFromCurrentMaxSequenceInstance = CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets()
          .toArray();
      boolean existsInDataSet = false;
      for (Object objectArrayFromASetId : arrayFromCurrentMaxSequenceInstance) {
        if (((String) ((Object[]) objectArrayFromASetId)[0]).equals(setId)) {
          // Set exists
          existsInDataSet = true;

          // When time has expired
          if (Duration.between(((Instant) ((Object[]) objectArrayFromASetId)[2]), Instant.now()).getSeconds() > 60
              * 15) {
            // Remove object from the CurrentMaxSequenceSet
            Collection<Object[]> currentMax = CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets();
            currentMax.remove(objectArrayFromASetId);
            CurrentMaxSequence.getInstance().setAllMaxValuesOfAllSets(currentMax);
            // Restart the request
            requestAndParseWrapper(setId);
            // fallback mode. If the status isnt 200 but there is an singleton. Put back the
            // old singleton information
            if (flashcardReturnStatusCode != 200) {
              addCollectionToSingleton(setId, ((String) ((Object[]) objectArrayFromASetId)[1]),
                  ((Instant) ((Object[]) objectArrayFromASetId)[2]));
              setMaxValueOfASet(Integer.parseInt(((String) ((Object[]) objectArrayFromASetId)[1])));
            }

          }
          // Set exists and time has not expired. Return the value from the CurrentMax
          else {
            setMaxValueOfASet(Integer.parseInt(((String) ((Object[]) objectArrayFromASetId)[1])));
          }
        }
      }
      // if the set doesn't exist, start the request
      if (!existsInDataSet) {
        requestAndParseWrapper(setId);
      }
    }
  }

  // wrapper
  private void requestAndParseWrapper(String setId) {
    try {
      parseRequestToMaxSequence(requestFlashcardServiceForTheMaxAvailable(setId), setId);
    } catch (IOException | InterruptedException e) {
      log.error(e.getMessage());
    }
  }

  // Request
  private HttpResponse<String> requestFlashcardServiceForTheMaxAvailable(String setId)
      throws IOException, InterruptedException {
    // connect to the flashcard service
    HttpRequest requestToFlashcardService = HttpRequest.newBuilder()
        .GET()
        .uri(URI.create(this.URL_FLASHCARD + "/api/flashcards/flashcard/maxavailable/" + setId))
        .setHeader("User-Agent", "Java 11 HttpClient Bot")
        .build();
    setRequestFromMemory(false);
    log.debug("Request to flashcard service.");
    return HTTP_CLIENT.send(requestToFlashcardService, HttpResponse.BodyHandlers.ofString());
  }

  // Parse Request
  private void parseRequestToMaxSequence(HttpResponse<String> responseFromFlashcardService, String setId) {
    if (responseFromFlashcardService.statusCode() == 200) {
      // parse response
      String[] cleanedBodyOfMaxValue = responseFromFlashcardService.body().replace("}", "").split(":");
      CurrentMaxSequence.getInstance().setCurrentFlashcardMaxSequence(Integer.parseInt(cleanedBodyOfMaxValue[1]));
      // update the requested max
      setMaxValueOfASet(Integer.parseInt(cleanedBodyOfMaxValue[1]));
      // add all the parsed information to the singleton
      addCollectionToSingleton(setId, cleanedBodyOfMaxValue[1], Instant.now());
    }
    // set the status code
    setFlashcardReturnStatusCode(responseFromFlashcardService.statusCode());
  }

  private void addCollectionToSingleton(String setId, String cleanedBody, Instant now) {
    // Current max sequence singleton
    Collection<Object[]> test = CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets();
    // object from parsed http request string
    Object[] tempList = { setId, cleanedBody, now };
    // add parsed object to max sequence singleton object
    test.addAll(Collections.singleton(tempList));
    // update the singleton
    CurrentMaxSequence.getInstance().setAllMaxValuesOfAllSets(test);
  }
}
