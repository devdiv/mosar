package nl.inquisitive.mosar.game.controller;

import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.game.controller.api.SequenceApiDelegate;
import nl.inquisitive.mosar.game.controller.model.GameSequence;
import nl.inquisitive.mosar.game.maxsequence.CurrentMaxSequence;
import nl.inquisitive.mosar.game.maxsequence.SetupAndUpdateMaxSequence;
import nl.inquisitive.mosar.game.sequencegenerator.SequenceGenerator;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@Slf4j
public class GameServiceController implements SequenceApiDelegate {

  @Autowired
  SetupAndUpdateMaxSequence requestTheFlashcardService;

  @Override
  public ResponseEntity<GameSequence> getGameSequence(String setId) {
    log.debug("Returning getGameSequence with max value "
        + CurrentMaxSequence.getInstance().getCurrentFlashcardMaxSequence());

    GameSequence gameSequence = new GameSequence();
    gameSequence.setSetid(setId);

    requestTheFlashcardService.singletonAllMaxValuesOfAllSetsHandler(setId);
    // If request is from memory or 200 return ok
    if (requestTheFlashcardService.getMaxValueOfASet() != 0) {
      log.debug("There is a max");
      if (requestTheFlashcardService.getFlashcardReturnStatusCode() != 200
          && !requestTheFlashcardService.isRequestFromMemory()) {
        log.warn("Using old information. Other service gave back the status "
            + requestTheFlashcardService.getFlashcardReturnStatusCode());
      }
      gameSequence.sequence(new SequenceGenerator(requestTheFlashcardService.getMaxValueOfASet()).getGameSequence());
      return ResponseEntity.ok().body(gameSequence);
    }
    // if nothing else return internal server error
    else {
      log.warn("Weird error from flashcards has been returned. Status is "
          + requestTheFlashcardService.getFlashcardReturnStatusCode());
      return ResponseEntity.internalServerError().build();
    }
  }
}
