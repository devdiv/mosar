[
  {
    "name": "flashcard",
    "image": "${FLASHCARD_IMAGE}:${FLASHCARD_IMAGE_TAG}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "mosar-flashcard-${ENVIRONMENT}-service",
        "awslogs-group": "${MOSAR_LOG_GROUP_NAME}"
      }
    },
    "portMappings": [
      {
        "containerPort": 8181,
        "hostPort": 8181,
        "protocol": "tcp"
      }
    ],
    "cpu": 1,
    "environment": [
      {
        "name": "SPRING_PROFILES_ACTIVE",
        "value": "${SPRING_PROFILES_ACTIVE}"
      },
      {
        "name": "DATABASE",
        "value": "${DATABASE}"
      },
      {
        "name": "DB_HOST",
        "value": "${DB_HOST}"
      },
      {
        "name": "DB_USER",
        "value": "${DB_USER}"
      },
      {
        "name": "TRUSTSTORE_PASS",
        "value": "${TRUSTSTORE_PASS}"
      }

    ],
    "secrets": [
    ],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 1024,
    "volumesFrom": [],
    "command": ["bash", "start-flashcards.sh"]
  }
]
