# flashcard app/variables.tf

variable "environment" {}
variable "spring_profiles_active" {}
variable "flashcard_image" {}
variable "flashcard_image_tag" {}
variable "database" {}
variable "db_host" {}
variable "db_user" {}
variable "truststore_pass" {}
variable "log_group_name" {}
variable "managed_by" {
  default = "jenkins_pipeline_flashcard"
}
