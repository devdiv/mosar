package nl.inquisitive.mosar.flashcard.controller;

import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
import nl.inquisitive.mosar.flashcard.repository.FlashcardEntity;
import nl.inquisitive.mosar.flashcard.repository.FlashcardsRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

class FlashcardControllerTest {

    @Mock
    FlashcardsRepository flashcardsRepository;

    @InjectMocks
    FlashcardController flashcardController;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }


    public void getFlashcard_Gives404NotFound_IfIdDoesNotExists() {
        //Setup data
        int id = 1000;
        //Setup mock
        Mockito.doThrow(javax.persistence.EntityNotFoundException.class).when(flashcardsRepository).getOne(Long.valueOf(id));

        //Run testingclass
        ResponseEntity<Flashcard> getFlashcardResponse = flashcardController.getFlashcard("1000","1");

        //Should return a 404 not found
        //Assertions.assertEquals("404 NOT_FOUND",getFlashcardResponse.getStatusCode().toString());
    }


    public void getFlashcard_Gives200Ok_IfIdExists() {
        //Setup Data
        FlashcardEntity flashcardEntity = new FlashcardEntity();
        flashcardEntity.setExplanation("Explanation");
        flashcardEntity.setTerm("Term");
        flashcardEntity.setId(1);
        int id = 1;

        //setup response
        Mockito.when(flashcardsRepository.getOne(Long.valueOf(id))).thenReturn(flashcardEntity);

        //Start testing class
        ResponseEntity<Flashcard> getFlashcardResponse = flashcardController.getFlashcard("1","1");

        //Assert
        Assertions.assertEquals("200 OK",getFlashcardResponse.getStatusCode().toString());
        Assertions.assertEquals("Term",getFlashcardResponse.getBody().getTerm());
        Assertions.assertEquals("Explanation",getFlashcardResponse.getBody().getExplanation());
    }
}