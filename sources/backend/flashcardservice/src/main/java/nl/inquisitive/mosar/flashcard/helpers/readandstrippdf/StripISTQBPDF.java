package nl.inquisitive.mosar.flashcard.helpers.readandstrippdf;


import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class StripISTQBPDF {

    @Setter
    @Getter
    List<String> allTermsList;

    @Getter
    @Setter
    List<String> allExplanationsList;

    public StripISTQBPDF(String strippedPDFString) {
        log.debug("start StripISTQBPDF");
        //remove the unneeded Page and references
        strippedPDFString = stripAllPageEnRef(strippedPDFString);

        //Pattern to match and match all that information in groups
        Pattern pattern = Pattern.compile("^[a-zERWV].*[^.]$(?=(\\n)[A-Z(]|(?=(\\n)(\\n)[A-Z(]))", Pattern.MULTILINE);
        Matcher matcher = pattern.matcher(strippedPDFString);

        //Get all the terms, remove new lines, filter empty and cast to list
        allTermsList = matcher.results().map(MatchResult::group).map(str -> str.replaceAll("\n", ""))
                .filter(str -> !str.matches("")).collect(Collectors.toList());
        //Get all the explanations in a pattern
        String[] allExplanations = pattern.split(strippedPDFString);
        //Get all the explanations remove new lines, filter empty and cast to list
        allExplanationsList = Arrays.stream(allExplanations).map(str -> str.replaceAll("\n", ""))
                .filter(str -> !str.matches("")).collect(Collectors.toList());
        log.debug("End StripISTQBPDF");
    }

    //Remove unneeded page:,ref:,synonyms:,See Also:.
    private String stripAllPageEnRef(String inputStringRaw) {
        return inputStringRaw.replaceAll("Page: [0-9]* of [0-9]*.*", "")
                .replaceAll("Ref: *.*", "")
                .replaceAll("Synonyms: *.*", "")
                .replaceAll("See Also: *.*", "")
                .replaceAll("\r", "");
    }
}