package nl.inquisitive.mosar.flashcard.helpers;


import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.ReadPSMJson;
import nl.inquisitive.mosar.flashcard.helpers.readandstripjson.StripPSMJson;
import nl.inquisitive.mosar.flashcard.helpers.readandstrippdf.ReadISTQBPDF;
import nl.inquisitive.mosar.flashcard.helpers.readandstrippdf.StripISTQBPDF;

import java.io.IOException;
import java.net.URISyntaxException;

@Slf4j
public class CreateSQLSeedingFile {
    public static void main(String[] args) throws URISyntaxException {
        log.debug("Starting StripISTQBFileToSQLDataFile");
        String rawPdfString = null;
        //read pdf and get a rawPDFstring
        try {
            rawPdfString = new ReadISTQBPDF(CreateSQLSeedingFile.class.getResource("/").getPath() + "istqb_foundation.pdf").getAllTextOFDocumentExceptFrontPage();


        } catch (IOException e) {
            log.error(e.getMessage());
        }

        StripPSMJson stripPSMJson =  new StripPSMJson(new ReadPSMJson(CreateSQLSeedingFile.class.getResource("/PSM1.json").toURI()).getExamModel());
        //pdf to text  //strip pdf
        StripISTQBPDF strippedTextFromPDF = new StripISTQBPDF(rawPdfString);

        //pupulatdatabse
        new nl.inquisitive.mosar.flashcard.helpers.sqlseed.CreateSQLSeedingFile(0,strippedTextFromPDF.getAllTermsList(),strippedTextFromPDF.getAllExplanationsList());
        new nl.inquisitive.mosar.flashcard.helpers.sqlseed.CreateSQLSeedingFile(1,stripPSMJson.getAllTermsList(),stripPSMJson.getAllExplanationsList());

        log.debug("End StripISTQBFileToSQLDataFile");
    }
}
