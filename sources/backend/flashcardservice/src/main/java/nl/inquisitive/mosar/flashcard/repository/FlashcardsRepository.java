package nl.inquisitive.mosar.flashcard.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FlashcardsRepository extends JpaRepository<FlashcardEntity, Long> {

    @Query(value = "SELECT * FROM flashcards.flashcard_entity WHERE setid = :setid AND flashcardid= :flashcardid", nativeQuery = true)
    FlashcardEntity getAFlashcardBySetIdAndFlashcardId(@Param("setid") String setId, @Param("flashcardid") String flashcardId);

    @Query(value = "SELECT count(id) FROM flashcards.flashcard_entity WHERE setid = :setid" , nativeQuery = true)
    int getMaxValueFromDatabase(@Param("setid") String setId);
}
