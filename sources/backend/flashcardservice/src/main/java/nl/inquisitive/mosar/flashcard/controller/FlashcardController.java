package nl.inquisitive.mosar.flashcard.controller;

import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.flashcard.controller.api.FlashcardApiDelegate;
import nl.inquisitive.mosar.flashcard.controller.model.Flashcard;
import nl.inquisitive.mosar.flashcard.controller.model.Maxflashcards;
import nl.inquisitive.mosar.flashcard.mapper.FlashcardMapper;
import nl.inquisitive.mosar.flashcard.repository.FlashcardEntity;
import nl.inquisitive.mosar.flashcard.repository.FlashcardsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class FlashcardController implements FlashcardApiDelegate {

  @Autowired
  FlashcardsRepository flashcardsRepository;

  @Override
  public ResponseEntity<Flashcard> getFlashcard(String setId, String flashcardId) {
    log.debug("Entering getFlashcard with ID" + flashcardId);
    // Get one card with ID from database if it exists
    try {

      FlashcardEntity flashcardEntity = flashcardsRepository.getAFlashcardBySetIdAndFlashcardId(setId, flashcardId);

      Flashcard flashcard = FlashcardMapper.INSTANCE.mapTo(flashcardEntity);
      log.debug("Returning OK for ID " + flashcardId);
      return ResponseEntity.ok().body(flashcard);
    } catch (Exception e) {
      // if there is an exception log
      log.error(e.getMessage());
      // if its unable to find it and it matches this string, return 404 else just
      // return a generic error
      if (e instanceof javax.persistence.EntityNotFoundException) {
        return ResponseEntity.notFound().build();
      } else {
        return ResponseEntity.badRequest().build();
      }
    }
  }

  // This is the post for creating flashcards. Disabled due to safety reasons.

  // @Override
  // public ResponseEntity<Flashcard> createFlashcard(Flashcard flashcard) {
  //
  // FlashcardEntity flashcardEntity = FlashcardMapper.INSTANCE.mapTo(flashcard);
  // FlashcardEntity savedFlashcard = flashcardsRepository.save(flashcardEntity);
  //
  // URI locationForSavedFlashcard =
  // ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedFlashcard.getId()).toUri();
  //
  // return ResponseEntity.created(locationForSavedFlashcard).build();
  // }

  @Override
  public ResponseEntity<Maxflashcards> getMaxAvailable(String setId) {
    log.debug("Starting getMaxValue");
    // Cast to Int from long
    int count = flashcardsRepository.getMaxValueFromDatabase(setId);
    // Create response instance
    Maxflashcards maxvalue = new Maxflashcards();
    maxvalue.setMaxFlashcards(count);
    // check if database has flashcards
    if (maxvalue.getMaxFlashcards() == 0) {
      // response if there are non is 404
      return ResponseEntity.notFound().build();
    } else {
      // Response is 200 with body
      return ResponseEntity.ok().body(maxvalue);
    }
  }
}
