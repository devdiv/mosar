package nl.inquisitive.mosar.flashcard;

import java.io.File;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

import nl.inquisitive.mosar.flashcard.security.SslParams;

import com.zaxxer.hikari.HikariDataSource;

@SpringBootApplication
@ComponentScan(basePackages = "nl.inquisitive.mosar.flashcard")
public class Application {

  @Autowired
  SslParams sslParams;

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Profile("aws")
  @Bean
  @ConfigurationProperties(prefix = "spring.datasource")
  public HikariDataSource dataSource() {
    HikariDataSource ds = DataSourceBuilder.create()
        .type(HikariDataSource.class)
        .build();
    return ds;
  }

  @PostConstruct
  void postConstruct() {

    // set TrustStoreParams
    File trustStoreFilePath = new File(sslParams.trustStorePath);
    String tsp = trustStoreFilePath.getAbsolutePath();
    System.setProperty("javax.net.ssl.trustStore", tsp);
    System.setProperty("javax.net.ssl.trustStorePassword", sslParams.trustStorePassword);
    System.setProperty("javax.net.ssl.keyStoreType", sslParams.defaultType);
  }
}
