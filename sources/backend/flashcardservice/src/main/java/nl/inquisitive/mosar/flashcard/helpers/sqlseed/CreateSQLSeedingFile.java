package nl.inquisitive.mosar.flashcard.helpers.sqlseed;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Slf4j
public class CreateSQLSeedingFile {


    public CreateSQLSeedingFile(int setid,List<String> inputAllTermsString, List<String> inputAllExplanationString) {
        //Information on the SQL file
        String sqlInputStatementString = "INSERT INTO `flashcards`.`flashcard_entity` (`setid`, `flashcardid`,`term`, `explanation`) VALUES (\"";
        //add resources datapath
        String sqlDataFileName = getClass().getResource("/").getPath() + "data.sql";

        //throw error if needed.
        if (inputAllExplanationString.size() != inputAllTermsString.size()) {
            log.error("terms and explanations are not the same size");
        }

        //create file
        try {
            //create file if not exists
            log.debug("Starting file writing.");
            File sqlDataFile = new File(sqlDataFileName);
            if (sqlDataFile.createNewFile()) {
                log.debug("File created: " + sqlDataFile.getName());
            } else {
                log.debug("File already exists. Thus will not be created.");
            }

            //Write SQL statements ot file
            FileWriter sqlDataFileWriter = new FileWriter(sqlDataFileName, true);
            for (int i = 0; i < inputAllTermsString.size(); i++) {
                sqlDataFileWriter.write(sqlInputStatementString + setid + "\", \"" + i + "\", \"" + inputAllTermsString.get(i) + "\", \"" + inputAllExplanationString.get(i) + "\")\n");
            }
            sqlDataFileWriter.close();
            log.debug("Successfully wrote to the file.");

        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
