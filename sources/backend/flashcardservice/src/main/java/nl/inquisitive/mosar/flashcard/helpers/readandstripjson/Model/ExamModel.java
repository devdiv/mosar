package nl.inquisitive.mosar.flashcard.helpers.readandstripjson.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ExamModel {

    @SerializedName("ExamInfo")
    @Expose
    private ExamInfoModel info;

    @SerializedName("ExamQuestions")
    @Expose
    private List<QuestionModel> questions;
}
