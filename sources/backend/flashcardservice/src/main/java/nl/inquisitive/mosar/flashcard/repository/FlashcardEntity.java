package nl.inquisitive.mosar.flashcard.repository;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "flashcard_entity")
public class FlashcardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    @Setter
    private long id;

    @Getter
    @Setter
    @Column
    private long setid;

    @Getter
    @Setter
    @Column
    private long flashcardid;

    @Getter
    @Setter
    @Lob
    @Column
    private String term;

    @Getter
    @Setter
    @Lob
    @Column
    private String explanation;

    @Getter
    @Setter
    private String createdate;

    @Getter
    @Setter
    private String createdby;

    @Getter
    @Setter
    private String changedby;
}
