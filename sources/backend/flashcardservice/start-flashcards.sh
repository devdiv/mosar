if [[ $TRUSTSTORE_PASS == "bla_truststore_pass" ]]
then
  echo "Running locally without installing rds cert"
  java -jar ./target/flashcards.jar
else
  echo "downloading rds  cert from aws"
  wget https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem
  echo "adding it to the truststore"
  keytool -importcert -alias rds-key -file rds-combined-ca-bundle.pem -keystore truststore -storepass $TRUSTSTORE_PASS -noprompt
  rm rds-combined-ca-bundle.pem
  echo "starting java app"
  java -jar ./target/flashcards.jar
fi


