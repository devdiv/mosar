if [ "${ENVIRONMENT}" = "prod" ]
then
  echo "starting frontend prod"
  cp /configs/prod.conf /etc/nginx/conf.d/default.conf
  mv /usr/share/nginx/html/config/config.prod.js /usr/share/nginx/html/config/config.js
else
  echo "starting frontend test"
  cp /configs/test.conf /etc/nginx/conf.d/default.conf
  mv /usr/share/nginx/html/config/config.test.js /usr/share/nginx/html/config/config.js
fi
nginx -g "daemon off;"
