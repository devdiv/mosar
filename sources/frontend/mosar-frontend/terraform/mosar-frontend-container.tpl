[
{
  "name": "${CONTAINER_NAME}",
    "image": "${FRONTEND_IMAGE}:${FRONTEND_IMAGE_TAG}",
    "essential": true,
    "logConfiguration": {
      "logDriver": "awslogs",
      "options": {
        "awslogs-region": "eu-central-1",
        "awslogs-stream-prefix": "mosar-frontend-${ENVIRONMENT}-service",
        "awslogs-group": "${MOSAR_LOG_GROUP_NAME}"
      }
    },
    "portMappings": [
      {
        "containerPort": 80,
        "hostPort": 80,
        "protocol": "tcp"
      }
    ],
    "cpu": 1,
    "environment": [
    ],
    "secrets": [
    ],
    "ulimits": [
      {
        "name": "nofile",
        "softLimit": 65536,
        "hardLimit": 65536
      }
    ],
    "mountPoints": [],
    "memory": 1024,
    "volumesFrom": []
  }
]
