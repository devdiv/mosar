import { BehaviorSubject, Observable } from 'rxjs';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { landingStateTrigger, landingTitleStateTrigger } from './landing-page.animations';
import { Router } from '@angular/router';
import { StateService } from '@mosar/app/shared/services/state.service';

interface Course {
  name: string;
  id: number;
}

const staticCourseArray: Course[] = [
  {
    name: 'ISTQB',
    id: 0,
  },
  {
    name: 'PSM1',
    id: 1,
  },
];
@Component({
  selector: 'mosar-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  animations: [landingStateTrigger, landingTitleStateTrigger],
})
export class LandingPageComponent {
  @ViewChild('courseModal') courseModal: ElementRef = {} as ElementRef;
  titleHover = false;
  showCourseModal = false;
  animationState = '';
  loadingInProgress = false;
  courses$: Observable<Course[]> = new BehaviorSubject<Course[]>(staticCourseArray);
  constructor(private stateService: StateService, private router: Router) {}
  navigate(setId: number): void {
    this.stateService.setId$.next(setId);
    this.router.navigate(['/flashcard']);
  }
  onClick(event: Event): void {
    if (this.showCourseModal && event.target !== this.courseModal.nativeElement) {
      this.showCourseModal = false;
    }
  }
}
