import {
  animate,
  AnimationTriggerMetadata,
  group,
  keyframes,
  query,
  sequence,
  style,
  transition,
  trigger,
} from '@angular/animations';

export const crashedStateTrigger: AnimationTriggerMetadata = trigger('crashedState', [
  transition(':enter', [
    style({
      opacity: 0,
    }),
    group([
      animate(
        '150ms ease-in',
        style({
          opacity: 1,
          transform: 'translateZ(10%)',
        }),
      ),

      query(
        '.not-found-page__area__top-dark-triangle',
        animate(
          '1500ms linear',
          keyframes([
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0.995,
            }),
            style({
              transform: 'translate(3%, 23%) rotateZ(47deg)',
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.not-found-page__area__top-light-triangle',
        animate(
          '1500ms linear',
          keyframes([
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0.995,
            }),
            style({
              transform: 'translateX(10%) rotateZ(40deg)',
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.not-found-page__area__top-grey-half-triangle',
        animate(
          '1500ms linear',
          keyframes([
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0.995,
            }),
            style({
              transform: 'translateX(0%) rotateZ(28deg)',
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.not-found-page__area__bottom-dark-triangle',
        animate(
          '1500ms linear',
          keyframes([
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0.995,
            }),
            style({
              transform: 'translateY(-25%) rotateZ(8deg)',
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.not-found-page__area__bottom-grey-half-triangle',
        animate(
          '1500ms linear',
          keyframes([
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0.995,
            }),
            style({
              transform: 'rotateZ(-15deg) translateX(-5%)',
              offset: 1,
            }),
          ]),
        ),
      ),
      query(
        '.not-found-page__area__bottom-light-triangle',
        animate(
          '1500ms linear',
          keyframes([
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(0) translate(0,0)',
              offset: 0.995,
            }),
            style({
              transform: 'translateY(0) rotateZ(30deg)',
              offset: 1,
            }),
          ]),
        ),
      ),

      sequence([
        style({
          transform: 'rotateZ(0)',
        }),
        animate(
          '1485ms',
          style({
            transform: 'rotateZ(0)',
          }),
        ),
        animate(
          '300ms ease-out',
          keyframes([
            style({
              transform: 'rotateZ(0)',
              offset: 0,
            }),
            style({
              transform: 'rotateZ(10deg)',
              offset: 0.1,
            }),
            style({
              transform: 'rotateZ(10deg)',
              offset: 0.2,
            }),
            style({
              transform: 'rotateZ(-8deg)',
              offset: 0.35,
            }),
            style({
              transform: 'rotateZ(6deg)',
              offset: 0.55,
            }),
            style({
              transform: 'rotateZ(-4deg)',
              offset: 0.8,
            }),
            style({
              transform: 'rotateZ(2deg)',
              offset: 1,
            }),
          ]),
        ),
      ]),
    ]),
  ]),
  transition(
    ':leave',
    animate(
      '300ms ease-out',
      style({
        opacity: 0,
      }),
    ),
  ),
]);
