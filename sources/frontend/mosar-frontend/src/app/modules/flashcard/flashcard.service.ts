import { Flashcard } from '@generated/flashcardservice/api/models/flashcard';
import { FlashcardApiService } from '@generated/flashcardservice/api/services/flashcard-api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FlashcardService {
  constructor(private flashcardApiService: FlashcardApiService) {}

  getFlashcard(setId: number, flashcardId: number): Observable<Flashcard> {
    return this.flashcardApiService.getFlashcard({ setid: setId.toString(), flashcardid: flashcardId.toString() });
  }
}
