import { RouterModule, Routes } from '@angular/router';
import { FlashcardComponent } from './flashcard.component';
import { NgModule } from '@angular/core';
import { SharedModule } from '@mosar/app/shared/shared.module';

const routes: Routes = [
  {
    path: '',
    component: FlashcardComponent,
  },
];

@NgModule({
  declarations: [FlashcardComponent],
  imports: [RouterModule.forChild(routes), SharedModule],
})
export class FlashcardModule {}
