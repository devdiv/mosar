import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { switchMap, takeUntil, tap } from 'rxjs/operators';
import { Router } from '@angular/router';

import { Flashcard } from '@generated/flashcardservice/api/models/flashcard';
import { FlashcardAnimationState } from './flashcard.animation-state.model';
import { FlashcardService } from './flashcard.service';
import { flashcardSideStateTrigger } from './flashcard.animations';
import { GameService } from '@mosar/app/services/game.service';
import { StateService } from '@mosar/app/shared/services/state.service';

@Component({
  selector: 'app-flashcard',
  templateUrl: './flashcard.component.html',
  styleUrls: ['./flashcard.component.scss'],
  animations: [flashcardSideStateTrigger],
})
export class FlashcardComponent implements OnInit, OnDestroy {
  isFrontSide = true;
  isLast = false;
  animationInProgress = false;
  loadingInProgress = true;
  animationState: FlashcardAnimationState = 'nothing';
  flashcard$!: Observable<Flashcard> | null;
  destroy$: Subject<boolean> = new Subject<boolean>();
  answer$ = new Subject<boolean>();

  constructor(
    private flashcardService: FlashcardService,
    private gameService: GameService,
    private stateService: StateService,
    private router: Router,
  ) {
    this.loadingInProgress = true;
  }

  ngOnInit(): void {
    const empty: Flashcard = { flashcardid: '', term: '', explanation: '' };
    this.gameService.loadSequence();

    this.flashcard$ = this.stateService.newSequenceState$.pipe(
      tap((gameSequence) => {
        this.isLast = gameSequence.sequence?.length === 1;
        this.isFrontSide = true;
      }),
      switchMap((gameSequence) =>
        gameSequence.setid && gameSequence.sequence && gameSequence.sequence.length > 0
          ? this.flashcardService.getFlashcard(+gameSequence.setid, gameSequence.sequence[0])
          : of(empty),
      ),
      tap((flashcard) => {
        if (flashcard !== empty) {
          this.loadingInProgress = false;
          this.triggerAnimation('new');
        }
      }),
    );

    this.answer$.pipe(takeUntil(this.destroy$)).subscribe((correct) => {
      const nextAnimation = correct ? 'right' : 'left';
      this.triggerAnimation(nextAnimation);
    });
  }

  flipSides(): void {
    if (!this.animationInProgress) {
      this.isFrontSide = !this.isFrontSide;
      this.triggerAnimation(this.isFrontSide ? 'front' : 'back');
    }
  }

  triggerAnimation(nextState: FlashcardAnimationState): void {
    this.animationState = nextState;
  }

  onDoneChooseNextAnimationState(): void {
    // gets triggered after animation is done, possibly choose a next state and animation
    // A state is either of type FlashcardAnimationState indicating both displayed info at rest as the animations.
    this.animationInProgress = true;
    switch (this.animationState) {
      case 'new': {
        this.triggerAnimation('front');
        this.animationInProgress = false;
        break;
      }
      case 'left':
      case 'right': {
        if (this.isLast) {
          this.router.navigate(['/']);
        } else {
          this.loadingInProgress = true;
          this.triggerAnimation('nothing');
          this.gameService.triggerNextFlashcard();
        }
        break;
      }
      case 'front':
      case 'back': {
        this.animationInProgress = false;
        break;
      }
      default: {
      }
    }
  }

  ngOnDestroy(): void {
    this.stateService.clearSequenceState();
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
