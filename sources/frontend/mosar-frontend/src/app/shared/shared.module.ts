import { ButtonsModule } from './modules/buttons/buttons.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';

@NgModule({
  declarations: [LoadingSpinnerComponent],
  imports: [CommonModule, ButtonsModule],
  exports: [CommonModule, ButtonsModule, LoadingSpinnerComponent],
})
export class SharedModule {}
