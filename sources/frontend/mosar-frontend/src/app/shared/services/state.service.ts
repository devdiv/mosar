import { GameSequence } from '@generated/gameservice/api/models';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class StateService {
  public newSequenceState$ = new ReplaySubject<GameSequence>(1);
  public setId$ = new ReplaySubject<number>(1);
  private readonly sequenceKey = 'sequence';
  private readonly setIdKey = 'setId';
  private _sequenceState: GameSequence | null = null;
  private _setId = -1;

  constructor() {
    this.newSequenceState$.subscribe((state) => localStorage.setItem(this.sequenceKey, JSON.stringify(state)));
    this.setId$.subscribe((setId) => {
      localStorage.setItem(this.setIdKey, setId.toString());
    });
  }

  get sequenceState(): GameSequence | null {
    if (this._sequenceState !== null) {
      return this._sequenceState;
    }
    this._sequenceState =
      localStorage.getItem(this.sequenceKey) != null ? JSON.parse(localStorage.getItem(this.sequenceKey) ?? '') : null;
    return this._sequenceState;
  }

  get setId(): number {
    if (this._setId > -1) {
      return this._setId;
    }
    this._setId =
      localStorage.getItem(this.setIdKey) != null ? JSON.parse(localStorage.getItem(this.setIdKey) ?? '') : -1;
    return this._setId;
  }

  clearSequenceState(): void {
    this._sequenceState = null;
    localStorage.removeItem(this.sequenceKey);
    this._setId = -1;
    localStorage.removeItem(this.setIdKey);
  }
}
