import { ButtonNegativeComponent } from './button-negative/button-negative.component';
import { ButtonPositiveComponent } from './button-positive/button-positive.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [ButtonPositiveComponent, ButtonNegativeComponent],
  imports: [CommonModule],
  exports: [ButtonNegativeComponent, ButtonPositiveComponent],
})
export class ButtonsModule {}
