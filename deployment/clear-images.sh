#!/bin/sh
set +e
be=$(docker image ls devdivision/mosar-flashcards -q)
fe=$(docker image ls devdivision/mosar-frontend -q)
[ ! -z $fe ] && docker image rm -f $fe
[ ! -z $be ] && docker image rm -f $be
set -e

