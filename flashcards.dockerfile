FROM devdivision/maven-node:v1.0.6 as build

WORKDIR /src
COPY ./sources/backend/pom.xml ./backend/pom.xml
COPY ./sources/openapi ./openapi
COPY ./sources/backend/flashcardservice/src ./backend/flashcardservice/src/
COPY ./sources/backend/flashcardservice/pom.xml ./backend/flashcardservice/pom.xml

WORKDIR /src/backend/flashcardservice

RUN mvn install

FROM openjdk:11.0.11-jdk-slim as runtime
RUN  apt-get update \
  && apt-get install -y wget
WORKDIR /app
COPY --from=build /src/backend/flashcardservice/target/flashcardservice*SNAPSHOT.jar ./target/flashcards.jar
ENV SPRING_PROFILES_ACTIVE=aws
ENV DB_HOST=bla_db_host
ENV DB_USER=bla_db_user
ENV DATABASE=bla_database
ENV TRUSTSTORE_PASS=bla_truststore_pass
CMD ["java", "-jar", "target/flashcards.jar"]


