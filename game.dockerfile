FROM devdivision/maven-node:v1.0.6 as build

WORKDIR /src
COPY ./sources/backend/pom.xml ./backend/pom.xml
COPY ./sources/openapi ./openapi
COPY ./sources/backend/gameservice/src ./backend/gameservice/src/
COPY ./sources/backend/gameservice/pom.xml ./backend/gameservice/pom.xml

WORKDIR /src/backend/gameservice

RUN mvn install

FROM openjdk:11.0.11-jdk-slim as runtime
WORKDIR /app
COPY --from=build /src/backend/gameservice/target/ ./target/
RUN mv ./target/gameservice*SNAPSHOT.jar ./target/game.jar
ENV SPRING_PROFILES_ACTIVE=aws
ENV SPRING_DATASOURCE_USERNAME=bla
ENV SPRING_DATASOURCE_PASSWORD=bla
CMD ["sh","-c","java -jar ./target/game.jar"]



